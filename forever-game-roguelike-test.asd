;;;; forever-game-roguelike-test.asd

(asdf:defsystem #:forever-game-roguelike-test
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :depends-on (:forever-game-roguelike-internal
               :prove
               :check-it)
  :pathname "test"
  :components ((:file "suite"))
  :description "Testing Forever Game Roguelike")
