;;;; forever-game-roguelike.lisp
;;;; Copyright (C) 2015 Lily Carpenter

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


(in-package #:forever-game-roguelike)

(defun start ()
  (sdl2:with-init (:everything)
    (sdl2:with-window (win :flags '(:shown :opengl))
      (sdl2:with-gl-context (gl-context win)
        (let ((controllers ())
              (haptic ()))
          (sdl2:gl-make-current win gl-context)
          (gl:viewport 0 0 800 600)
          (gl:matrix-mode :projection)
          (gl:ortho -2 2 -2 2 -2 2)
          (gl:matrix-mode :modelview)
          (gl:load-identity)
          (gl:clear-color 0.0 0.0 1.0 1.0)
          (gl:clear :color-buffer)

          (sdl2:with-event-loop (:method :poll)
            (:idle
             ()
             (gl:clear :color-buffer)
             (gl:begin :triangles)
             (gl:color 1.0 0.0 0.0)
             (gl:vertex 0.0 1.0)
             (gl:vertex -1.0 -1.0)
             (gl:vertex 1.0 -1.0)
             (gl:end)
             (gl:flush)
             (sdl2:gl-swap-window win))

            (:keyup
             (:keysym keysym)
             (when (sdl2:scancode= (sdl2:scancode-value keysym) :scancode-escape)
               (sdl2:push-event :quit)))

            (:quit () t)))))))
