* Phase Two: Base builder
The purpose of this phase is to experiment with the RPG + RTS/simulation aspects combined with realtime
simulation and turn based combat.
** Features
- Basic melee combat
- Basic ranged combat
- HP
- Ability to designate tasks (build X, gather Y, etc)
- NPC workers that that do the actual work
- NPC guards that will fight for you
- Super basic resource system
- Procedurally generated terrain
- Periodic enemy attacks
** Resources
- Wood (from trees)
- Food (from farms)
** Buildings
- Wall
- Door
- Roof
- Farm
- Stockpile
