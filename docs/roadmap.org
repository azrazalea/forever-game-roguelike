* General thoughts

I expect the road to 1.0 to be very very long. Probably multiple years.

That being said, keeping a up-to-date roadmap should help me stay organized.

This is a LIVING document, so please don't assume this will never change.

They are also in some rough chronological order but the order is by no means guaranteed.

Each version will be tagged upon being considered complete
* Research
** TODO ECS systems :realworld:
- Excellent Intro: http://cowboyprogramming.com/2007/01/05/evolve-your-heirachy/
- The most cited paper: http://scottbilas.com/files/2002/gdc_san_jose/game_objects_slides.pdf
  _Note_ I didnt get as much out of this one ^^ at the time. Im not sure why as it's pretty
  thorough.
- Entity Systems are the future of MMOG development:
  http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-2/
  http://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-3/
  I loved this one (and I have no interest in mmorpgs), it was my favourite after the first one I linked. It was the first one where I 'got' some of the advantages.
- Entitas: https://www.youtube.com/watch?v=1wvMXur19M4
  This is an alternative ECS for Unity, it uses a precompile pass for C# which it makes it's approach interesting as that maps straight to macros.
  Very focussed on performance, which is one of the reasons I like ECS.
** TODO Read through http://gameprogrammingpatterns.com/contents.html :realworld:
:PROPERTIES:
:Effort:   20
:END:
* Code Snippets (for future use)
** How to get a path to executable or project depending on which you are running as:
from([[https://github.com/mfiano/gamebox/blob/master/src/utils/utils.lisp]])
#+BEGIN_SRC lisp
(in-package :gbx.util)

(defun path (context dir &optional file)
  (let ((path (format nil "~A/~@[~A~]" dir file)))
    (with-slots (user) context
      (if uiop/image:*image-dumped-p*
          (truename
           (uiop/pathname:merge-pathnames*
            path
            (uiop:pathname-directory-pathname (uiop:argv0))))
          (asdf/system:system-relative-pathname user path)))))

(defun read-file (context file)
  (let ((path (path context "." file)))
    (when (and (or (pathnamep path) (stringp path))
               (uiop/filesystem:file-exists-p path))
      (with-open-file (in path)
        (let ((*read-eval* nil))
          (read in))))))

(defun flatten-floats (sequence)
  (let ((sequence (mapcar #'float (flatten sequence))))
    (make-array (length sequence)
                :element-type 'single-float
:initial-contents sequence)))
#+END_SRC
* Versions
** 0.0
*** DONE Ask #lispgames for feedback on this roadmap.           :realworld:
CLOSED: [2016-09-20 Tue 19:35]
:PROPERTIES:
:Effort:   1
:END:
*** TODO Figure out a name for the game                         :realworld:
:PROPERTIES:
:Effort:   1
:END:
- Magisterium
- Praeditus
- Opus Perpetuum (thanks aeth from #lispgames for the suggestion)
*** TODO Learn ncurses
- http://www.ibiblio.org/pub/Linux/docs/HOWTO/other-formats/html_single/NCURSES-Programming-HOWTO.html
- http://stevelosh.com/blog/2016/08/lisp-jam-postmortem/#ncurses-and-cl-charms
- https://github.com/sjl/sand/blob/master/src/ascii.lisp#L12-L91
*** ABORTED Go through opengl tutorials to become more comfortable :realworld:
CLOSED: [2016-09-20 Tue 19:35]
:PROPERTIES:
:Effort:   60
:END:
** 0.1
*** TODO Implement basic ncurses 2D graphics system with tiles and character "graphics". Should be overhead view, not isomorphic. Similar to dwarf fortress. :client:
:PROPERTIES:
:Effort:   40
:END:
**** Possibly use someone else's engine, though that loses some learning value.
*** TODO Implement basic player character with movement and basic melee and ranged attack (no RNG yet, just standard) as well as health. :client:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Create basic static map to play around in. Implement this by creating a map format to save maps and loading it. Should have walls, nothing else. :client:
:PROPERTIES:
:Effort:   10
:END:
*** TODO Add basic tile based collision so player can't exit map nor collide with walls. :client:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Create basic config. Basic player stats (health, attack damage) should exist. :client:
:PROPERTIES:
:Effort:   10
:END:
*** TODO Create one enemy, dummy with melee attack and health. Make sure it also has collision. No AI needed for this version, just a punching bag. :client:
:PROPERTIES:
:Effort:   10
:END:
**** Training dummy graphic would be good. Perhaps an auto retaliate attack to test out combat better?
** 0.2
*** TODO Ask on #lispgames for feedback on version 0.1.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Move game to basic client <-> server model over local sockets. :client:server:
:PROPERTIES:
:Effort:   80
:END:
**** The only reason this is so early is to attempt to avoid coding myself into a corner.
**** Long term I want this to be protocol agnostic and allow multi clients per a server.
** 0.3
*** TODO Ask on #lispgames for feedback on version 0.2.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Flesh out character design ideas, start prototyping in org-mode/paper/whiteboard character talents. :realworld:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Add items                                                   :server:
:PROPERTIES:
:Effort:   20
:END:
*** TODO Add some items to static map.                               :server:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Add item placeholder graphics and text. :client:
*** TODO Add inventory to player/enemies.                          :server:
*** TODO Add inventory view                                        :client:
*** TODO Add ability for player to pickup items                    :client:
**** The server side of this should have been done in Add inventory to player/enemies above.
*** TODO Add basic AI enemy that will attack and run away. :server:
** 0.4
*** TODO Ask on #lispgames for feedback on version 0.3.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
** 0.5
*** TODO Ask on #lispgames for feedback on version 0.4.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
** 0.6
*** TODO Ask on #lispgames for feedback on version 0.5.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
** 0.7
*** TODO Ask on #lispgames for feedback on version 0.6.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :server:client:
:PROPERTIES:
:Effort:   20
:END:
** 0.8
*** TODO Ask on #lispgames for feedback on version 0.7.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :server:client:
:PROPERTIES:
:Effort:   20
:END:
** 0.9
*** TODO Ask on #lispgames for feedback on version 0.8.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :server:client:
:PROPERTIES:
:Effort:   20
:END:
** 1.0
*** TODO Ask on #lispgames for feedback on version 0.9.         :realworld:
:PROPERTIES:
:Effort:   1
:END:
*** TODO Implement #lispgames feedback that I like. :client:server:
:PROPERTIES:
:Effort:   20
:END:
