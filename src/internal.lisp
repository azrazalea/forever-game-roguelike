;;;; internal.lisp
;;;; Copyright (C) 2016 Lily Carpenter

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Affero General Public License for more details.

;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package #:forever-game-roguelike-internal)

(defstruct game-state
  (cursor-x 0 :type fixnum)
  (cursor-y 0 :type fixnum)
  (quit? nil :type boolean))

(defun player-up (state)
  (decf (game-state-cursor-y state)))

(defun player-down (state)
  (incf (game-state-cursor-y state)))

(defun player-left (state)
  (decf (game-state-cursor-x state)))

(defun player-right (state)
  (incf (game-state-cursor-x state)))

(defun quit-game (state)
  (setf (game-state-quit? state) t))

(defun handle-input (char state)
  (case char
    ((#\w) (player-up    state))
    ((#\s) (player-down  state))
    ((#\a) (player-left  state))
    ((#\d) (player-right state))
    ((#\q #\Q) (quit-game state))))

(defun start (&optional (state (make-game-state)))
  (with-curses ()
    (disable-echoing)
    (enable-raw-input)
    ;; TODO: The extra keys don't actually work in cl-charms, fix this.
    (enable-extra-keys *standard-window*)
    (loop named game-loop
       for char := (get-char *standard-window* :ignore-error t) do
         (refresh-window *standard-window*)
         (handle-input char state)
         (multiple-value-bind (width height) (window-dimensions *standard-window*)
           (setf (game-state-cursor-x state) (mod (game-state-cursor-x state) width))
           (setf (game-state-cursor-y state) (mod (game-state-cursor-y state) height)))
         (move-cursor *standard-window* (game-state-cursor-x state) (game-state-cursor-y state))
       until (game-state-quit? state))))

(defun start-dev (state)
  (ql:quickload :slynk)
  (uiop:symbol-call :slynk
                    :create-server
                    :port 6666
                    :dont-close t)
  (start state))
