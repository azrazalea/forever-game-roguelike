;;;; forever-game-roguelike.asd

(asdf:defsystem #:forever-game-roguelike
  :description "The start of my forever project game."
  :author "Lily Carpenter <lily-license@azrazalea.net>"
  :license "AGPLv3"
  :serial t
  :depends-on ("cl-charms"
               "split-sequence"
               "alexandria")
  :pathname "src"
  :components ((:file "package")
               (:file "internal")))
